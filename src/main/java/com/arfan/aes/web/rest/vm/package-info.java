/**
 * View Models used by Spring MVC REST controllers.
 */
package com.arfan.aes.web.rest.vm;
