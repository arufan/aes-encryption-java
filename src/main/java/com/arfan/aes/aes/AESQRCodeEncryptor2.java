package com.arfan.aes.aes;

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import java.util.Base64;

public class AESQRCodeEncryptor2 {

    private String TOKEN;
    private String salt;
    private int pwdIterations = 65536;
    private int keySize = 256;
    private byte[] ivBytes;
    private String keyAlgorithm = "AES";
    private String encryptAlgorithm = "AES/CBC/PKCS5Padding";
    private String secretKeyFactoryAlgorithm = "PBKDF2WithHmacSHA1";
    private String payload;

    public AESQRCodeEncryptor2(){}

    public AESQRCodeEncryptor2(PayloadRequest request) {
        this.TOKEN = request.getToken();
        this.salt = request.getSalt();
        this.payload = request.getPayload();
    }

    public String encrypt() throws Exception {
        byte[] saltBytes = this.salt.getBytes("UTF-8");
        byte[] iv = new byte[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        IvParameterSpec ivspec = new IvParameterSpec(iv);
        SecretKeyFactory skf = SecretKeyFactory.getInstance(this.secretKeyFactoryAlgorithm);
        PBEKeySpec spec = new PBEKeySpec(this.TOKEN.toCharArray(), saltBytes, this.pwdIterations, this.keySize);
        SecretKey secretKey = skf.generateSecret(spec);
        SecretKeySpec key = new SecretKeySpec(secretKey.getEncoded(), this.keyAlgorithm);
        Cipher cipher = Cipher.getInstance(this.encryptAlgorithm);
        cipher.init(1, key, ivspec);
        this.ivBytes = ((IvParameterSpec)cipher.getParameters().getParameterSpec(IvParameterSpec.class)).getIV();
        byte[] encryptedText = cipher.doFinal(this.payload.getBytes("UTF-8"));
        return Base64.getEncoder().encodeToString(encryptedText);
    }

    public String decrypt() throws Exception {
        byte[] saltBytes = this.salt.getBytes("UTF-8");
        byte[] iv = new byte[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        IvParameterSpec ivspec = new IvParameterSpec(iv);
        byte[] encryptTextBytes = Base64.getDecoder().decode(this.payload);
        SecretKeyFactory skf = SecretKeyFactory.getInstance(this.secretKeyFactoryAlgorithm);
        PBEKeySpec spec = new PBEKeySpec(this.TOKEN.toCharArray(), saltBytes, this.pwdIterations, this.keySize);
        SecretKey secretKey = skf.generateSecret(spec);
        SecretKeySpec key = new SecretKeySpec(secretKey.getEncoded(), this.keyAlgorithm);
        System.out.println("key:" + Base64.getEncoder().encodeToString(key.getEncoded()));
        Cipher cipher = Cipher.getInstance(this.encryptAlgorithm);
        cipher.init(2, key, ivspec);
        byte[] decyrptTextBytes = null;

        try {
            decyrptTextBytes = cipher.doFinal(encryptTextBytes);
        } catch (IllegalBlockSizeException var12) {
            var12.printStackTrace();
        } catch (BadPaddingException var13) {
            var13.printStackTrace();
        }

        String text = new String(decyrptTextBytes);
        return text;
    }
}
