package com.arfan.aes.aes;

public class PayloadRequest {

    private  String payload;
    private  String token;
    private  String salt;


    public PayloadRequest(){

    }

    public PayloadRequest(String payload, String token, String salt) {
        this.payload = payload;
        this.token = token;
        this.salt = salt;
    }

    public String getPayload() {
        return this.payload;
    }

    public String getToken() {
        return this.token;
    }

    public String getSalt() {
        return this.salt;
    }

    public String toString() {
        return "PayloadRequest [payload=" + this.payload + ", token=" + this.token + ", salt=" + this.salt + "]";
    }
}
